// Caches files, like images and webms that are embedded to posts

var path = require("path");
var fs = require("fs");
var request = require("request");

module.exports = {
    cacheUrl: function(url, callback) {
        // Create dir if does not exist
        fs.mkdir("imgcache", function() {});

        var filename = path.basename(url);

        request({url: url, encoding: null}, function(error, response, body) {
            //imgcache.put(name, body);
            fs.writeFile("imgcache/" + filename, body);

            callback(filename);
        });
    },
    getCachedFile: function(name) {
        return fs.readFileSync("imgcache/" + name);
    },
    isFileCached: function(name) {
        return fs.existsSync("imgcache/" + name);
    }
}
