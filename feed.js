var chan = require('4chanjs');
var Future = require("async-future");

function Thread(board, number, lastPostNumber) {
    this.board = board;
    this.number = number;
}
Thread.prototype.getPostsAfterNumber = function(number) {
    var future = new Future();
    var that = this;

    chan.board(this.board).thread(this.number, function(err, posts) {
        if (err) {
            future.throw(err);
            return;
        }
        if (posts == undefined) {
            future.throw("posts is undefined");
            return;
        }

        var filteredPosts = posts.filter(function(post) {
            return post.no > number;
        }).sort(function(a, b) {
            return a.no - b.no;
        }).map(function(post) {
            return {
                threadNo: that.number,

                time: post.time,
                no: post.no,
                com: post.com,

                filename: post.filename,
                tim: post.tim,
                ext: post.ext,
                md5: post.md5
            }
        });

        future.return(filteredPosts);
    });

    return future;
}

module.exports = {
    findActiveThread: function(boardname, filter) {
        var future = new Future();

        var board = chan.board(boardname);
        board.catalog(function(err, pages) {
            if (err) {
                future.throw(err);
                return;
            }

            if (!pages) {
                future.throw("no pages returned");
                return;
            }

            if(!pages.reduce) {
                future.throw("pages.reduce false??");
                return;
            }

            var allThreads = pages.reduce(function(a, b) {
                return a.concat(b.threads);
            }, []);

            var wantedThreads = allThreads.filter(filter).sort(function(a, b) {
                var a_reply_times = (a.last_replies) ? a.last_replies.map(function(reply) { return reply.time; }) : [];
                var b_reply_times = (b.last_replies) ? b.last_replies.map(function(reply) { return reply.time; }) : [];

                // No replies on either
                if (a_reply_times.length == 0 && b_reply_times.length == 0) {
                    return 0;
                }

                return b_reply_times[0] - a_reply_times[0];
            });

            if (wantedThreads.length == 0) {
                future.throw("No threads that pass the filter found");
                return;
            }

            future.return(new Thread(boardname, wantedThreads[0].no));
        });

        return future;
    }
}
