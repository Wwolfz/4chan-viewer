var http = require("http");
var io = require("socket.io");
var fs = require("fs");
var feed = require("./feed");
var filecache = require("./filecache");
var mime = require("mime");

var serverFiles = fs.readdirSync("http/");

var app = http.createServer(function(req, res) {
    var url = req.url;
    if (url == "/") { url = "/client.html"; }

    if (url.indexOf("/img/") == 0) {
        var imgsrc = url.substring(5);

        if (!filecache.isFileCached(imgsrc)) {
            res.writeHead(404);
            res.end("");
            return;
        }

        var val = filecache.getCachedFile(imgsrc);
        if (val) {
            res.writeHead(200);
            res.end(val);
            return;
        }
    }

    for (var idx in serverFiles) {
        var file = serverFiles[idx];

        if (url == "/" + file) {
            fs.readFile("http/" + file, function(err, data) {
                res.writeHead(200);
                res.end(data);
            });
            return;
        }
    }
    res.writeHead(404);
    res.end("File not found: " + url);
});

app.listen(8080);
var io_app = io.listen(app);

var d2gThreadFilter = function(thread) {
    var lowsub = thread.sub.toLowerCase();
    return lowsub.indexOf("d2g") != -1 || lowsub.indexOf("dota") != -1;
}

var latestPosts = [];

io_app.sockets.on("connection", function(socket) {
    latestPosts.forEach(function(p) {
        p.fileCached = filecache.isFileCached(p.tim + p.ext);
        socket.emit("newPost", JSON.stringify(p));
    });
})

var lastPost;
var lastActiveThread;
function fetchNewestPosts() {
    feed.findActiveThread("vg", d2gThreadFilter).then(function(activeThread) {
        if (lastActiveThread != undefined && lastActiveThread != activeThread.number) {
            console.log("Active thread from " + lastActiveThread + " to " + activeThread.number);
        }
        lastActiveThread = activeThread.number;
        return activeThread.getPostsAfterNumber(lastPost == undefined ? 0 : lastPost);
    }).then(function(newPosts) {
        newPosts.forEach(function(post) {
            if (post.filename) {
                post.mime = mime.lookup(post.tim+post.ext);

                filecache.cacheUrl("http://i.4cdn.org/vg/src/" + post.tim + post.ext, function(name) {
                    io_app.sockets.emit("imageCached", JSON.stringify({name: name, no: post.no}));
                });
            }

            io_app.sockets.emit("newPost", JSON.stringify(post));

            var newLength = latestPosts.push(post);
            if (newLength > 50) {
                latestPosts.shift();
            }
        });

        if (newPosts.length > 0) {
            lastPost = newPosts.slice(-1)[0].no;
        }
    }).catch(function(e) {
        console.log("feed error: ", e);
    });
}

fetchNewestPosts();
setInterval(fetchNewestPosts, 5000);
