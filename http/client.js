var socket = io.connect("http://localhost");

var scrollToBottom = _.throttle(function() {
    $('html, body').animate({scrollTop: $(document).height()}, 'slow');
}, 200);

function repliesToQuotelinks(replies) {
    return replies.map(function(o) {
        return "<a href=\"#p" + o + "\" class=\"quotelink\">" + o + "</a>";
    }).join(", ");
}

var templateString =
    '<span class="filecontainer">' +
    '<% if (post.filename){ %>' +
        '<% if (post.mime.indexOf("image/") == 0) { %>' +
            '<img id="filep<%= post.no %>" class="file filesrc" src="<% if (post.fileCached) { print("img/"+post.filepath); } %>">' +
        '<% } else if (post.mime == "video/webm") {%>' +
            '<video id="filep<%= post.no %>" class="file" autoplay loop><source class="filesrc" src="<% if (post.fileCached) { print("img/"+post.filepath); } %>" type="video/webm"></video>' +
        '<% } %>' +
    '<% } %>' +
    '</span>' +
    '<div class="post"><%= post.comment %></div>' +
    '<span class="postno post-metadata"><%- post.no %><br><%- post.formattedtime %><br><a href="http://boards.4chan.org/vg/thread/<%- post.threadNo %>#p<%- post.no %>" target="_blank">Go to</a></span>' +
    '<span class="postreplies post-metadata"><% ' +
        'print(repliesToQuotelinks(post.replies)) ' +
    '%></span>'
;
var postTemplate = _.template(templateString);

socket.on("newPost", function (data) {
    var jsonobj = JSON.parse(data);
    console.log(jsonobj);

    var shouldScrollToBottom = $(window).scrollTop() + $(window).height() > $(document).height() - 25;

    if ($("#file-preview").is(":visible") == true)
        shouldScrollToBottom = false;

    var messagebox = $("<div>")
                        .addClass("messagebox")
                        .addClass("messagecontainer")
                        .attr("id", "p" + jsonobj.no)
                        .attr("name", "p" + jsonobj.no);

    var date = new Date(jsonobj.time);

    var pad = function(n) { if ((n+"").length == 2) return n; else return "0" + n; }
    var datestr = pad(date.getHours()) + ":" + pad(date.getMinutes()) + ":" + pad(date.getSeconds());

    var postobj = {
        threadNo: jsonobj.threadNo,
        no: jsonobj.no,
        comment: jsonobj.com,

        formattedtime: date.toLocaleTimeString(),

        fileCached: jsonobj.fileCached,
        filename: jsonobj.filename + jsonobj.ext,
        filepath: jsonobj.tim + jsonobj.ext,
        mime: jsonobj.mime,

        replies: []
    };

    messagebox.data("postobj", postobj);
    messagebox.html(postTemplate({post: postobj}));

    messagebox.hide().appendTo($("#container")).fadeIn();

    if(shouldScrollToBottom) {
        scrollToBottom();
    };

    updateReplies(postobj);
});

socket.on("imageCached", function(data) {
    var _data = JSON.parse(data);

    $("#filep" + _data.no).attr("src", "img/" + _data.name);

    var postobj = $("#p" + _data.no).data("postobj");
    postobj.fileCached = true;

    console.log("File cached: ", _data.name);
})

function updateRepliesInMessage(msgelement) {
    msgelement.find(".postreplies").html(repliesToQuotelinks(msgelement.data("postobj").replies));
}
function updateReplies(post) {
    if (post.comment == undefined) {
        return;
    }
    var unescaped = post.comment.replace(/&gt;/g, ">");
    var re = />>(\d+)/g;

    var replies = [];

    var match;
    while (match = re.exec(unescaped)) {
        replies.push(match[1]);
    }

    replies = _.uniq(replies);

    _.each(replies, function(replyno) {
        var msg = $("#p" + replyno);
        if (msg.length != 0) {
            msg.data("postobj").replies.push(post.no);
            updateRepliesInMessage(msg);
        }
    })

    console.log("Post " + post.no + " has replies to following posts: " + replies);
}

jQuery.fn.selfOrChildren = function(selector) {
    if ($(this).is(selector)) {
        return $(this);
    }
    return $(this).find(selector);
}

$(document).on("click", ".messagecontainer .file", function() {
    window.open($(this).selfOrChildren(".filesrc").attr("src"), "_blank");
});
$(document).on("mouseenter", ".messagecontainer .file", function() {
    // TODO postobj null on post-preview
    var postobj = $(this).parent().parent().data("postobj");

    $("#file-preview").html($(this).parent().html()).append($("<span>").html(postobj != undefined ? postobj.filename : ""));
    $("#file-preview").show();
});
$(document).on("mouseleave", ".messagecontainer .file", function() {
    $("#file-preview").hide();
});

$(document).on("mouseenter", ".messagecontainer a.quotelink", function() {
    var targetName = $(this).attr("href").substring(1);
    var targPost = $("#" + targetName);

    targPost.addClass("outlined-post");

    if (targPost.length != 0) {
        $("#post-preview").html(postTemplate({post: targPost.data("postobj")})).show();
    }
});
$(document).on("mouseleave", ".messagecontainer a.quotelink", function() {
    $(".outlined-post").removeClass("outlined-post");
    $("#post-preview").hide();
});
$(document).on("click", ".post>a.quotelink", function(e) {
    var targetName = $(this).attr("href").substring(1);
    var targPost = $("#" + targetName);

    if ($(this).next().is("div.messagecontainer")) {
        $(this).next().remove();
    }
    else {
        $(this).after($("<div>").addClass("messagecontainer").addClass("inline-preview").html(postTemplate({post: targPost.data("postobj")})));
    }

    e.preventDefault();
});

$(document).on("mouseenter", ".messagecontainer .postno", function(e) {
    $(this).animate({
        height: "+=50"
    }, 100);
});
$(document).on("mouseleave", ".messagecontainer .postno", function(e) {
    $(this).animate({
        height: "-=50"
    }, 100);
});

var clamp = function(num, min, max) {
    return num < min ? min : (num > max ? max : num);
};

$(document).on("mousemove", function(event) {
    $("#post-preview").width(function() {
        return $(window).width() * 0.8;
    });

    $(".floating-preview").css("left", function() {
        return clamp(event.pageX + 15, 0, $(window).width()-$(this).width());
    }).css("top", function() {
        return clamp(event.pageY - $(this).height() - 15, $(window).scrollTop(), $(window).scrollTop()+$(document).height() - $(this).height());
    });
});
