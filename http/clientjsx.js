// http://stackoverflow.com/a/26152067
var If = React.createClass({
	render: function() {
		if (this.props.test) {
			return this.props.children;
		}
		else {
			return false;
		}
	}
});

var FCPostFile = React.createClass({
	render: function() {
		return (
			<img src={this.props.src} />
		);
	}
});

var FCPostThumbnail = React.createClass({
	getInitialState: function() {
		return {
			expanded: false
		};
	},
	onMouseEnter: function(e) {
		if (this.state.expanded) {
			return;
		}
		React.render(React.createElement(FCPostFile, {src: this.props.src}), document.getElementById("container-filepreview"));
	},
	onMouseLeave: function(e) {
		React.unmountComponentAtNode(document.getElementById("container-filepreview"));
	},
	onClick: function(e) {
		this.setState({expanded: !this.state.expanded});
	},
	render: function() {
		return (
			<div onClick={this.onClick} className={this.state.expanded ? "" : "post-thumbnail"}>
				<FCPostFile src={this.props.src} />
			</div>
		);
	}
});

function intersperse(arr, sep) {
	if (arr.length === 0) {
		return [];
	}

	return arr.slice(1).reduce(function(xs, x, i) {
		return xs.concat([sep, x]);
	}, [arr[0]]);
}
var FCPost = React.createClass({
	render: function() {
		var leftFloat = {
			float: "left"
		};
		var rightFloat = {
			float: "right"
		};

		var createPostLink = function(id) {
			return <a key={this.props.id + "reply" + id} className="quotelink" href={"#p" + id}>{id}</a>;
		}.bind(this);

		return (
			<div id={"p" + this.props.id} className="post">
				<If test={this.props.file && this.props.file.loaded}><FCPostThumbnail src={this.props.file && this.props.file.src} /></If>
				<div className="post-info">
					<div style={leftFloat}>
						{this.props.id}
						<a href={"http://boards.4chan.org/" + this.props.board + "/thread/" + this.props.threadId + "#p" + this.props.id} target="_blank">Original</a> 
						<If test={this.props.file}><a href={this.props.file && ("https://www.google.com/searchbyimage?&image_url=http://i.4cdn.org/" + this.props.board + "/" + this.props.file.osrc)} target="_blank">RevSrch</a></If>
					</div>
					<div style={rightFloat}>{this.props.quotedBy && intersperse(this.props.quotedBy.map(createPostLink), ", ")}</div>
				</div>
				<div dangerouslySetInnerHTML={{__html: this.props.text}} />
			</div>
		);
	}
});

var FCPostList = React.createClass({
	render: function() {
		var createPost = function(post, index) {
			return <FCPost key={post.id} {...post} />
		};
		return (
			<div>{this.props.posts.map(createPost)}</div>
		);
	}
});


function parseReplies(com) {
	var unescaped = com.replace(/&gt;/g, ">");
	var re = />>(\d+)/g;

	var replies = [];

	var match;
	while (match = re.exec(unescaped)) {
		replies.push(parseInt(match[1]));
	}

	replies = _.uniq(replies);
	return replies;
}
var FCFeedApp = React.createClass({
	getInitialState: function() {
		return {
			posts: []
		};
	},
	getPostById: function(id) {
		return _.filter(this.state.posts, function(x) { return x.id == id; })[0];
	},
	addPost: function(post) {
		var that = this;

		this.setState({posts: this.state.posts.concat([post])}, function() {
			// Figure out what posts did this post reply to
			var replies = parseReplies(post.text);
			_.each(replies, function(replyno) {
				var replyToPost = that.getPostById(replyno);
				if (replyToPost) {
					replyToPost.quotedBy = replyToPost.quotedBy || [];
					replyToPost.quotedBy.push(post.id);
				}
			});
		});
	},
	render: function() {
		return (
			<div>
				<FCPostList posts={this.state.posts} />
			</div>
		);
	}
});

var comp = React.render(<FCFeedApp />, document.getElementById("container"));

var scrollToBottom = _.throttle(function() {
	var shouldScrollToBottom = $(window).scrollTop() + $(window).height() > $(document).height() - 25;
	if (!shouldScrollToBottom) {
		return;
	}

	$('html, body').animate({scrollTop: $(document).height()}, 'slow');
}, 200);

var socket = io.connect("http://localhost");
socket.on("newPost", function(data) {
	var parsed = JSON.parse(data);
	parsed.com = parsed.com || "";

	var hasFile = parsed.tim != undefined;
	var isFileLoaded = parsed.fileCached;

	var post = {
		board: "vg",
		threadId: parsed.threadNo,
		id: parsed.no,
		text: parsed.com
	};

	if (hasFile) {
		var osrc = parsed.tim + parsed.ext;

		post.file = {
			osrc: osrc,
			src: "img/" + osrc,
			loaded: isFileLoaded
		};
	}

	comp.addPost(post);

	scrollToBottom();
});
socket.on("imageCached", function(data) {
	var parsed = JSON.parse(data);
	comp.getPostById(parsed.no).file.loaded = true;
});

$(document).on("mousemove", function(e){
	$("#container-postpreview").css({
	   left:  e.pageX + 2,
	   top:   e.pageY
	});
});

$(document).on("mouseenter", "a.quotelink", function(e) {
	var targetName = $(this).attr("href");
	var id = parseInt(targetName.substring(2));

	var post = comp.getPostById(id);
	if (!post) {
		return;
	}

	React.render(React.createElement(FCPost, post), document.getElementById("container-postpreview"));
});
$(document).on("mouseleave", "a.quotelink", function(e) {
	React.unmountComponentAtNode(document.getElementById("container-postpreview"));
});